package ru.t1.godyna.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.model.IWBS;
import ru.t1.godyna.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "name", nullable = false, length = 50)
    private String name = "";

    @NotNull
    @Column(name = "descrptn", nullable = false, length = 2000)
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "project_id", nullable = true, length = 50)
    private String projectId;

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    public TaskDTO(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(@NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
